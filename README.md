# logback-flume-appender #

### What is this repository for? ###

* Custom Logback appender that writes logs to an Apache Flume agent.

### Running tests ###

* There are two test classes:
    * FlumeAppenderTest - mocks out the part that talks to flume so can be run anytime.
    * LogbackXmlTest - uses a logback-test.xml file that configures a flume server:port; the test is disabled but can be enabled if the flume process is running.