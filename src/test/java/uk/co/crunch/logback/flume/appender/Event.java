package uk.co.crunch.logback.flume.appender;

import java.util.Date;

public class Event {

    private final String eventType;
    private final Client client;
    private final String action;
    private final Date date;

    public Event(String eventType, Client client, String action, Date date) {
        this.eventType = eventType;
        this.client = client;
        this.action = action;
        this.date = date;
    }

    public String getEventType() {
        return eventType;
    }

    public Client getClient() {
        return client;
    }

    public String getAction() {
        return action;
    }

    public Date getDate() {
        return date;
    }
}
