package uk.co.crunch.logback.flume.appender;

public class Client {

    private final Long id;
    private final String firstname;
    private final String lastname;

    public Client(Long id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
