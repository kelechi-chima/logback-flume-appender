package uk.co.crunch.logback.flume.appender;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.Date;

public class LogbackXmlTest {

    private static final Logger AUDIT_LOGGER = LoggerFactory.getLogger("AUDIT_LOGGER");

    @Test(enabled = false, description = "The logger defined in logback-test.xml writes events to a flume agent which must be running.")
    public void exerciseLoggerConfiguredInLogbackXml() {
        final Client c = new Client(1234L, "Luther", "Blissett");
        final Event e = new Event("UPDATE", c, "Updated dividend record", new Date());
        AUDIT_LOGGER.info(new Gson().toJson(e));
    }
}
